# Project Delta
A Bakugan game made by and for fans

## Folder structure
This project's divided into several folders. These being game, important, plot and promo.

### Artwork
This folder houses concept art and promotional material for this project.

### Game
This folder houses the current RPG Maker project. Currently, we are using RPG Maker MV, but this may change in the future.

### Imported
This folder houses assets imported from other Bakugan games like *Battle Brawlers* and *Defenders of the Core*. The files may be located in a ZIP file.

### Installer
This folder houses the custom graphical user interface for the installer of the game which will be used in production.

### Plot
This folder houses the current plans for the plot. It may change as the project evolves over time.

### Site
This folder houses the files for the website of this project.
