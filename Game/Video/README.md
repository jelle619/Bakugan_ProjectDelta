So far, a way to play video in Wine (macOS and Linux port) has not
been found. Until then, please avoid overly depending on the playback of videos.
The following text only applies to the native Windows version of the game.

# Video
This folder houses the video files that will/may be used in-game. Keep the
following in mind.

## Codec
You will need the WM9 codec to make the video play in-game. This codec usually
comes included with Windows, but if for whatever reason it you don't have it,
you may download it here: https://bit.ly/2pCAB4j. Video support is provided by
the Zeus Video Player script. The author of this script recommends you to
install Xvid (https://www.xvid.com/) if video playback still does not work for
whatever reason.

## Format
Video files must be in WMV format for optimal compatibility, as this file format
is part of the WM9 codec which comes installed by default on most Windows
installations. Please don't use other video formats. For more information about
which codecs Windows supports by default visit https://bit.ly/2P6rhQj.

## Compression
Do not compress your video files! It's important for your video to have a
quality as high as possible.

## Resolution
In general, try to stick with a resolution of 640 (width) by 480 (height).
